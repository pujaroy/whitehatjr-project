import axios from 'axios';
let customerClient = undefined;
export function getCustomerClient() {
    customerClient = customerClient || axios.create({
        baseURL:"https://jsonplaceholder.typicode.com",
        timeout: 10000,
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        }
    });
    return customerClient;
}