import {getCustomerClient} from './client.js';
export function fetchCustomers() {
    return getCustomerClient().get('/users');
}
