import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/home.vue";
import Services from "../views/services.vue";
import Works from "../views/works.vue";
import Contact from "../views/contact.vue";
import Blog from "../views/blog.vue";
Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: Home,
    component: Home
  },
  {
    path: "/inventions",
    name: Services,
    component: Services
  },
  {
    path: "/awards",
    name: Works,
    component: Works
  },
  {
    path: "/personal-life",
    name: Contact,
    component: Contact
  },
  {
    path: "/Legacy-and-honors",
    name: Blog,
    component: Blog
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;